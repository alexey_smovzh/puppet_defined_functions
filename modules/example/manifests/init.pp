class example {

  $root = lookup('root', String)
  $folder = lookup('folder', String)
  $file = lookup('file', String)


  example::utils::folder { 'create_root':
    folder => $root
  }

  example::utils::folder { 'create_folder':
    folder => "${root}/${folder}",
    require => Example::Utils::Folder['create_root']
  }

  example::utils::file { 'create_file':
    filename => "${root}/${folder}/${file}",
    owner => 'alex',
    content => template('example/example.erb'),
    require => Example::Utils::Folder['create_folder']
  }

}
