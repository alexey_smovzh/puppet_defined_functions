# Create file with provided content
#
# @filename     - file name with full path e.g. '/root/new_file'
# @owner    - optional parameter: file owner
# @content  - file content, can be string content or link to template
# @require  - optional parameter: requirements to run this function
#

define example::utils::file(
  String $filename, 
  Optional[String] $owner = undef, 
  String $content,
  Optional[Any] $require = undef
  ) {

  file { $filename:
    ensure => 'present',
    owner => $owner,
    group => $owner,
    content => $content,
    require => $require
  }

}
