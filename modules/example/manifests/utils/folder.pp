# Creates folder
#
# @folder   - folder path and name for example: '/root/folder'
# @owner    - optional parameter: folder owner
# @require  - optional parameter: requirements to run this function
#

define example::utils::folder(
  String $folder, 
  Optional[String] $owner = undef, 
  Optional[Any] $require = undef
  ) {

  file { $folder:
    ensure => 'directory',
    owner => $owner,
    group => $owner,
    require => $require
  }

}
